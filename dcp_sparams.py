from skrf import Frequency, Network
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from scipy.interpolate import RegularGridInterpolator
from scipy.signal import find_peaks
import matplotlib.gridspec as gridspec
import skrf as rf

# desired analyses:
# - generate scattering matrix as a function of te 
DATA_DIR = '/home/IPP-HGW/aliiss/projects/git/icrh-dcp/data/' #'/home/issraali/ipp/git/icrh-dcp/data/'
MIN_LENGTH = 0
MAX_LENGTH = 1500
LENGTH_GAP = 50
MIN_FREQ = 20000000
MAX_FREQ = 40000000
FREQ_GAP = 12500
D_INPT = 343 + 270
c = 3e8  # speed of light in m/s

def rotate_all(length_1:float, length_2:float):
    '''
    rotate port 1 `length_1` mm towards load, 
    rotate port 2 `length_2` mm away from load. 
    save new files in a directory `data_rotated`. 
    '''
    os.mkdir('data_rotated')
    for file in os.listdir('./data'):
        network = pd.read_csv(DATA_DIR + file, sep = '\t', header = 4,
                              names = ['f', 's11_mag', 's11_phase', 's21_mag', 's21_phase', 's12_mag', 's12_phase','s22_mag', 's22_phase'])
        elec_length_1_deg = (360 * network.f * length_1)/c
        network.s11_phase = network.s11_phase + elec_length_1_deg
        elec_length_2_deg = (360 * network.f * length_2)/c
        network.s22_phase = network.s22_phase - elec_length_2_deg

        elec_length_total_deg = (360 * network.f * (length_1 + length_2))/c
        network.s12_phase = network.s12_phase + elec_length_total_deg
        network.s21_phase = network.s21_phase + elec_length_total_deg

        
        network.to_csv('./data_rotated/' + file, sep = '\t', header = None, index = False)
        data = open('./data_rotated/' + file, 'r').read()
        origfile = open(DATA_DIR + file)

        newfile =open('./data_rotated/' + file, 'w')
        for line in origfile.readlines()[0:5]:
            newfile.write(line)

        newfile.write(data)
        newfile.close()

    return None

def s_param_callable(datapath: str = DATA_DIR):          #(sxx: str, f: float, l: float, datapath: str = './data_rotated'):
    '''
    Outputs callable interpolant function for the desired S-parameter. 
    The arguments of the interpolant function are f [Hz] and l [mm]. 
    '''
    flist = os.listdir(datapath)
    flist.sort()
    for counter, file in enumerate(flist):
        temp = pd.read_csv(DATA_DIR + file, sep = '\t', header = 4,
                names = ['f', 's11_mag', 's11_phase', 's21_mag', 's21_phase', 's12_mag', 's12_phase','s22_mag', 's22_phase'])
        temp['l'] = float(file[:-6]) * np.ones_like(temp.f)
        
        if counter == 0:
            network = temp

        else:
            network = pd.concat([network, temp])

    network = network.sort_values(['l', 'f'], ascending= [True, True])
    # network = network[(network.f - MIN_FREQ) % 500000 == 0]
    # print(network)
    

    freq = network[network.l == 0]['f'].to_numpy()
    length = network[network.f == 20000000]['l'].to_numpy()

    outs = []

    for col in network:
        var_array = network[col].to_numpy()
        var_array = var_array.reshape(len(length), len(freq))
        #print(var_array)

        interp = RegularGridInterpolator((length, freq), var_array, method = 'quintic', bounds_error=True)
        outs.append(interp)

    return outs[1:-1]

def ideal_stub(f, l, Z0 = 50):
    l = 4722 + 1.7589959582360706 * l #1.97862898e+00 * l #play around with 2
    elec_l = (2 * np.pi * f * l * 0.001)/c
    elec_d_in = (2 * np.pi * f * D_INPT * 0.001)/c

    Z_short = Z0 * 1j*np.tan(elec_l)
    Z_vna = Z0
    
    Z_t = 1/((1/Z_short) + (1/Z_vna))
    Z_in = Z0 * (Z_t + 1j*Z0*np.tan(elec_d_in))/(Z0 + 1j*Z_t*np.tan(elec_d_in))

    s11 = (Z_in - Z0)/(Z_in + Z0)

    ###############################
    mag = 20 * np.log10(np.abs(s11))
    phase = np.angle(s11, deg = True)

    return mag

def real_component_lin(db, deg):
    mag = 10 ** (db/20)
    phase_rad = deg * np.pi/180
    
    return mag * np.cos(phase_rad)

def im_component_lin(db, deg):
    mag = 10 ** (db/20)
    phase_rad = deg * np.pi/180
    
    return mag * np.sin(phase_rad)

def mod_ideal_stub(f, l, Z):
    z0 = 50
    l2 = 712 + 2 * l 
    l1 = 1770 # first bit 
    l3 = 2270 # to short
    elec_l1 = (2 * np.pi * f * l2 * 0.001)/c
    elec_l2 = (2 * np.pi * f * l1 * 0.001)/c 
    elec_l3 = (2 * np.pi * f * l3 * 0.001)/c 
    elec_d = (2 * np.pi * f * D_INPT * 0.001)/c

    # calculating z line
    zA = 1j*z0*np.tan(elec_l3)
    zB = Z * (zA + 1j * Z * np.tan(elec_l1))/(Z + 1j * zA * np.tan(elec_l1))
    zC = z0 * (zB + 1j * z0 * np.tan(elec_l2))/(z0 + 1j * zB * np.tan(elec_l2))

    Z_short = zC #Z0 * 1j*np.tan(elec_l)
    Z_vna = z0
    
    Z_t = 1/((1/Z_short) + (1/Z_vna))
    Z_in = z0 * (Z_t + 1j*z0*np.tan(elec_d))/(z0 + 1j*Z_t*np.tan(elec_d))

    s11 = (Z_in - z0)/(Z_in + z0)

    ###############################
    mag = 20 * np.log10(np.abs(s11))

    return mag

def main():

    freq_domain = np.arange(MIN_FREQ, MAX_FREQ, FREQ_GAP)
    
    fig = plt.figure(figsize = (14, 8))
    gs = gridspec.GridSpec(4, 1, hspace = 0.2)
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[1, 0])#(gs[1, :])
    ax3 = fig.add_subplot(gs[2, 0])
    ax4 = fig.add_subplot(gs[3, 0])

    axlist = [ax1, ax2, ax3, ax4]

    for i, L in enumerate([0, 500, 1000, 1495]):
        length_list = L * np.ones_like(freq_domain)
        s11_mag, s11_phase, s21_mag, s21_phase, s12_mag, s12_phase, s22_mag, s22_phase = s_param_callable()
        mod_45 = mod_ideal_stub(freq_domain, L, 45)
        mod_50 = mod_ideal_stub(freq_domain, L, 50)
        mod_best = mod_ideal_stub(freq_domain, L, 57.18243429471282)
        mod_60 = mod_ideal_stub(freq_domain, L, 60)
        axlist[i].plot(freq_domain, s11_mag((length_list, freq_domain)), label = 'measured value')
        axlist[i].plot(freq_domain, mod_45, label = '$Z0_{{ext}} = 45 \Omega$', linestyle = '--')
        axlist[i].plot(freq_domain, mod_50, label = '$Z0_{{ext}} = 50 \Omega$', linestyle = '--')
        axlist[i].plot(freq_domain, mod_best, label = '$Z0_{{ext}} = 57.18 \Omega$', linestyle = '--')
        axlist[i].plot(freq_domain, mod_60, label = '$Z0_{{ext}} = 60 \Omega$', linestyle = '--')
        axlist[i].set_ylabel('S11 magnitude [dB]')
        axlist[i].set_title(f"$\ell_{{ext}} = {L} mm$")
        if i != 3:
            axlist[i].get_xaxis().set_ticks([])
    
    axlist[-1].legend()
    axlist[-1].set_xlabel('Frequency [Hz]')

    # # ax4 = fig.add_subplot(gs[2, 1])
    # # ax5 = fig.add_subplot(gs[2, 2])
    # # ax6 = fig.add_subplot(gs[3, :])
    # # ax7 = fig.add_subplot(gs[3, 1])
    # # ax8 = fig.add_subplot(gs[3, 2])

    # #data = rf.Network('/home/issraali/ipp/git/icrh-dcp/data/1000MM.S2P')

    # ##############################
    # # Plotting for l = 400 mm 
    # ax2.plot(freq_domain, s11_mag((length_list, freq_domain)), label = 'measured value')
    # #ax2.plot(freq_domain, ideal_mag, label = 'ideal', linestyle = '--')
    # #x2.plot(freq_domain, ideal_mag, label = 'data', linestyle = '-')

    # #ideal = ideal_stub(freq_domain, L)
    # mod_45 = mod_ideal_stub(freq_domain, L, 45)
    # mod_50 = mod_ideal_stub(freq_domain, L, 50)
    # mod_55 = mod_ideal_stub(freq_domain, L, 55)
    # mod_60 = mod_ideal_stub(freq_domain, L, 60)

    # #ax2.plot(freq_domain, ideal, label = 'ideal model', linestyle = '--')
    # ax2.plot(freq_domain, mod_45, label = 'Z0 = 45', linestyle = '--')
    # ax2.plot(freq_domain, mod_50, label = 'Z0 = 50', linestyle = '--')
    # ax2.plot(freq_domain, mod_55, label = 'Z0 = 55', linestyle = '--')
    # ax2.plot(freq_domain, mod_60, label = 'Z0 = 60', linestyle = '--')

    # ax2.set_xlabel('frequency')
    # ax2.set_ylabel('magnitude [dB]')

    ##############################
    # 2D plots
    # freq_fine = np.linspace(MIN_FREQ, MAX_FREQ, 4000)
    # length_fine = np.linspace(0, 1450, 2000)
    # lg, fg = np.meshgrid(length_fine, freq_fine, indexing='ij')

    # test_points = np.array([lg.ravel(), fg.ravel()]).T
    # im_exp  = s11_mag(test_points).reshape(len(length_fine), len(freq_fine))
    # ax3.pcolormesh(freq_fine, length_fine, im_exp)
    # ax3.set_xlabel('frequency [Hz]')
    # ax3.set_ylabel('decoupler extension (mm)')

    # im_ideal, phase = ideal_stub_callable(fg, lg)
    # im_ideal = im_ideal.reshape(len(length_fine), len(freq_fine))
    # ax4.pcolormesh(freq_fine, length_fine, im_ideal)
    # ax4.set_xlabel('frequency [Hz]')
    # ax4.set_ylabel('decoupler extension (mm)')

    # modified = mod_line(fg, lg, 0)
    # modified = modified.reshape(len(length_fine), len(freq_fine))
    # ax5.pcolormesh(freq_fine, length_fine, modified)
    # ax5.set_xlabel('frequency [Hz]')
    # ax5.set_ylabel('decoupler extension (mm)')

    # modified = mod_line(fg, lg, 48)
    # modified = modified.reshape(len(length_fine), len(freq_fine))
    # ax6.pcolormesh(freq_fine, length_fine, modified)
    # ax6.set_xlabel('frequency [Hz]')
    # ax6.set_ylabel('decoupler extension (mm)')

    # modified = mod_line(fg, lg, 50)
    # modified = modified.reshape(len(length_fine), len(freq_fine))
    # ax7.pcolormesh(freq_fine, length_fine, modified)
    # ax7.set_xlabel('frequency [Hz]')
    # ax7.set_ylabel('decoupler extension (mm)')

    # modified = mod_line(fg, lg, 52)
    # modified = modified.reshape(len(length_fine), len(freq_fine))
    # ax8.pcolormesh(freq_fine, length_fine, modified)
    # ax8.set_xlabel('frequency [Hz]')
    # ax8.set_ylabel('decoupler extension (mm)')

    # data.plot_s_db()

    ##############################
    # Generating list of passbands for each length

    # ideal_min_list = []
    # measured_min_list = []

    # for length in range(0, 1500, 50):
    #     measured_stopband = np.argmin(s11_mag((length, freq_domain)))
        
    #     ideal_mag, ideal_phase = ideal_stub_callable(freq_domain, length)
    #     ideal_stopband = np.argmin(ideal_mag)

    #     ideal_min_list.append(freq_domain[ideal_stopband])
    #     measured_min_list.append(freq_domain[measured_stopband])


    # ax1.plot(range(0, 1500, 50), ideal_min_list, label = 'ideal')
    # ax1.plot(range(0, 1500, 50), measured_min_list, label = 'measured')
    # ax1.set_xlim(400, 1500)
    # ax1.legend()
    
    plt.savefig('mod.png', dpi = 400)
    plt.show()

if __name__ == "__main__":
    main()