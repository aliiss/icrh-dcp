from dcp_sparams import *
from scipy.optimize import curve_fit, minimize_scalar

def s_param_data(datapath = DATA_DIR):
    '''
    Outputs callable interpolant function for the desired S-parameter. 
    The arguments of the interpolant function are f [Hz] and l [mm]. 
    '''
    flist = os.listdir(datapath)
    flist.sort()
    for counter, file in enumerate(flist):
        temp = pd.read_csv(DATA_DIR + file, sep = '\t', header = 4,
                names = ['f', 's11_mag', 's11_phase', 's21_mag', 's21_phase', 's12_mag', 's12_phase','s22_mag', 's22_phase'])
        temp['l'] = float(file[:-6]) * np.ones_like(temp.f)
        
        if counter == 0:
            network = temp

        else:
            network = pd.concat([network, temp])

    network = network.sort_values(['l', 'f'], ascending= [True, True])
    network = network[(network.f - MIN_FREQ) % 500000 == 0]
    print(network)
    

    freq = network[network.l == 0]['f'].to_numpy()
    length = network[network.f == 20000000]['l'].to_numpy()

    outs = []

    for col in network:
        var_array = network[col].to_numpy()
        var_array = var_array.reshape(len(length), len(freq))
        #print(var_array)
        outs.append(var_array)
    return freq, length, outs[1:-1]

def mod_ideal_stub(X, Z):
    z0 = 50
    #Z = 50
    l, f = X
    l1 = 641.5 + 2 * l #play around with 2
    l2 = 2122
    elec_l1 = (2 * np.pi * f * l1 * 0.001)/c
    elec_l2 = (2 * np.pi * f * l2 * 0.001)/c
    elec_l3 = (2 * np.pi * f * 2300 * 0.001)/c
    elec_d = (2 * np.pi * f * D_INPT * 0.001)/c

    # calculating z line
    zA = 1j*z0*np.tan(elec_l3)
    zB = Z * (zA + 1j * Z * np.tan(elec_l1))/(Z + 1j * zA * np.tan(elec_l1))
    zC = z0 * (zB + 1j * z0 * np.tan(elec_l2))/(z0 + 1j * zB * np.tan(elec_l2))

    Z_short = zC #Z0 * 1j*np.tan(elec_l)
    Z_vna = z0
    
    Z_t = 1/((1/Z_short) + (1/Z_vna))
    Z_in = z0 * (Z_t + 1j*z0*np.tan(elec_d))/(z0 + 1j*Z_t*np.tan(elec_d))

    s11 = (Z_in - z0)/(Z_in + z0)

    ###############################
    mag = 20 * np.log10(np.abs(s11))

    return mag

freq, length, arr = s_param_data()
s11_mag, s11_phase, s21_mag, s21_phase, s12_mag, s12_phase, s22_mag, s22_phase = arr

lg, fg = np.meshgrid(length, freq, indexing='ij')
X = np.array([lg.ravel(), fg.ravel()])
print(X)
s11_mag = s11_mag.flatten()
print(s11_mag)

print((mod_ideal_stub(X, 2)))

error_real = lambda Z: np.sum((10**(mod_ideal_stub(X, Z)/10) - 10**(s11_mag/10))**2)
error_db = lambda Z: np.sum((mod_ideal_stub(X, Z) - s11_mag)**2)


Z_real = minimize_scalar(error_real, bracket=(0, 100), method='brent')
print(Z_real)
Z_db = minimize_scalar(error_db, bracket=(0, 100), method='brent')
print(Z_db)
#print((mod_ideal_stub(X, 1.75899)))


# popt, pcov = curve_fit(mod_ideal_stub, X, s11_mag, p0 = 1.97)
# print(popt)

